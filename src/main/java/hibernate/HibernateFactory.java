package hibernate;

import java.util.HashMap;
import java.util.Map;

import entities.DocumentsEntity;
import entities.PersonsEntity;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

public class HibernateFactory {

    private static final SessionFactory SESSION_FACTORY;

    static {
        Configuration configuration = HibernateFactory.provideClassConfigurationSetup();
        StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

        registryBuilder
                .applySettings(configuration.getProperties())
                .applySettings(HibernateFactory.provideSetupSettings());
        SESSION_FACTORY = configuration.buildSessionFactory(registryBuilder.build());
    }

    public static SessionFactory getSessionFactory() {
        return SESSION_FACTORY;
    }

    public static void close() {
        SESSION_FACTORY.close();
    }

    private static Map<String, String> provideSetupSettings() {
        Map<String, String> dbSettings = new HashMap<>();
        dbSettings.put(Environment.DRIVER, "org.postgresql.Driver");
        dbSettings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");

        dbSettings.put(Environment.URL, "jdbc:postgresql://localhost:5432/medicine");
        dbSettings.put(Environment.USER, "postgres");
        dbSettings.put(Environment.PASS, "superpassword");
        return dbSettings;
    }

    private static Configuration provideClassConfigurationSetup() {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(PersonsEntity.class);
        configuration.addAnnotatedClass(DocumentsEntity.class);
        return configuration;
    }
}
