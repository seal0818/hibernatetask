
import entities.DocumentsEntity;
import entities.PersonsEntity;
import hibernate.HibernateFactory;
import org.hibernate.Session;

import javax.persistence.criteria.*;
import java.util.List;

public class HibernateTask {

    public static final String QUERY_STRING =
            "SELECT p FROM persons p INNER JOIN p.documents d WHERE d.documentNumber LIKE '%777%' AND active = true";

    public static void main(String[] args) {
        Session session = HibernateFactory.getSessionFactory().openSession();
        List<PersonsEntity> list =  session.createQuery(QUERY_STRING, PersonsEntity.class).getResultList();
        for (Object personEntyty : list) {
           System.out.println(personEntyty.toString());
        }
        session.close();
        HibernateFactory.close();
    }
}
