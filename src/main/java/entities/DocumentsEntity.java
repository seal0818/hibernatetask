package entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity(name = "documents")
@Table(name = "documents", schema = "public", catalog = "medicine")
public class DocumentsEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;
    @Basic
    @Column(name = "document_type")
    private String documentType;
    @Basic
    @Column(name = "active")
    private boolean active;
    @Basic
    @Column(name = "person_id")
    private UUID personId;
    @Basic
    @Column(name = "document_number")
    private String documentNumber;

    public UUID getId() {
        return this.id;
    }

    public DocumentsEntity setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getDocumentType() {
        return documentType;
    }

    public DocumentsEntity setDocumentType(String documentType) {
        this.documentType = documentType;
        return this;
    }

    public boolean isActive() {
        return this.active;
    }

    public DocumentsEntity setActive(boolean active) {
        this.active = active;
        return this;
    }

    public UUID getPersonId() {
        return this.personId;
    }

    public DocumentsEntity setPersonId(UUID personId) {
        this.personId = personId;
        return this;
    }

    public String getDocumentNumber() {
        return this.documentNumber;
    }

    public DocumentsEntity setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
        return this;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        DocumentsEntity that = (DocumentsEntity) o;
        return this.active == that.active && this.personId == that.personId
                && Objects.equals(this.id, that.id)
                && Objects.equals(this.documentType, that.documentType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, documentType, active, personId);
    }

    @Override
    public String toString() {
        return  "Document ID:"      + this.id + " " +
                "Document type: "   + this.documentType + " " +
                "Active: "          + this.active + " " +
                "Document number: " + this.documentNumber;
    }

}
