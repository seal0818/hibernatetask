package entities;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Basic;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity(name = "persons")
@Table(name = "persons", schema = "public", catalog = "medicine")
public class PersonsEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;
    @Basic
    @Column(name = "first_name")
    private String firstName;
    @Basic
    @Column(name = "second_name")
    private String secondName;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "person_id")
    private List<DocumentsEntity> documents = new ArrayList<>();

    public UUID getId() {
        return this.id;
    }

    public PersonsEntity setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return this.secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public List<DocumentsEntity> getDocuments() {
        return this.documents;
    }

    public void setDocuments(List<DocumentsEntity> documents) {
        this.documents = documents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || this.getClass() != o.getClass())
            return false;
        PersonsEntity that = (PersonsEntity) o;
        return this.id == that.id && Objects.equals(this.firstName, that.firstName)
                && Objects.equals(this.secondName, that.secondName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, secondName);
    }

    @Override
    public String toString() {
        return  "Person ID: "    + this.id + " " +
                "First name: "   + this.firstName + " " +
                "Second name: "  + this.secondName + " " +
                "Documents: "    + this.documents.stream()
                                                .map(DocumentsEntity::toString)
                                                .collect(Collectors.joining(", "));
    }
}
